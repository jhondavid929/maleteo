<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MaleteoFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    //Definicion del Formulario
    {
        $builder->add('nombre', TextType::class);


        $builder->add('email', EmailType::class);


        $builder->add('horario', TextType::class);



        $builder->add('ciudad', ChoiceType::class);

        $builder->add('Privacidad', CheckboxType::class);

        $builder->add('enviar', SubmitType::class);
        
    }
}