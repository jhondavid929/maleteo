<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PeliculaRepository")
 */
class Pelicula
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Titulo;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Descuripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->Titulo;
    }

    public function setTitulo(string $Titulo): self
    {
        $this->Titulo = $Titulo;

        return $this;
    }

    public function getDescuripcion(): ?string
    {
        return $this->Descuripcion;
    }

    public function setDescuripcion(string $Descuripcion): self
    {
        $this->Descuripcion = $Descuripcion;

        return $this;
    }
}
