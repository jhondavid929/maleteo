<?php

namespace App\Controller;

use App\Form\MaleteoFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class MaleteoController extends AbstractController
{
    /**
     * @Route("/maleteo")
     */
    public function maleteo(EntityManagerInterface $emi, Request $request) {

        $form = $this->createForm(MaleteoFormType::class);

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
            $formulario = $form->getData();
            $emi->persist($formulario);
            $emi->flush();
    
        }

        return $this->render("maleteo/maleteo.html.twig",
            [
                'formulario'=>$form->createView()
            ]
            );
    }

}



