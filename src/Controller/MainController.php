<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController 
{
    /**
     * @Route("/contact")
     */

    public function contact () 
    {
        $respuesta = new Response("MI PRIMERA RUTA");

        return $respuesta;
    }


    /**
     * @Route("/saluda/{nombre}/{ape}",  methods= {" POST", "GET"})
     */

    public function saluda($nombre, $ape)
    {
        $variables = [
            'nombre' => $nombre,
            'apellidos' => $ape 
        ];

        return $this->render('index.html.twig', $variables);
        

        
    }







}